let rooms = [
  {
    name: 'bedroom',
    func: [
      {
        name: ['light', 'lights'],
        option: ['on', 'off'],
        status: 'off'
      },
      {
        name: ['alarm'],
        option: ['on', 'off'],
        status: 'off'
      }
    ],
  },
  {
    name: 'livingroom',
    func: [
      {
        name: ['light', 'lights'],
        option: ['on', 'off'],
        status: 'off'
      },
      {
        name: ['heater'],
        option: ['on', 'off'],
        status: 'off'
      },
      {
        name: ['tv', 'television'],
        option: ['on', 'off'],
        status: 'off'
      }
    ],
  },
  {
    name: 'kitchen',
    func: [
      {
        name: ['coffemachine'],
        option: ['on', 'off'],
        status: 'off'
      },
      {
        name: ['light', 'lights'],
        option: ['on', 'off'],
        status: 'off'
      }
    ],
  },
];



// Search for Keywords in a sentence
function searhKeyWords(txt) {
  txt = txt.split(' ');
  let room = checkRooms(txt);
  if (room) {
    console.log(room.name);
    let func = checkFunc(room, txt);
    if (func) {
      console.log(func[1]);
      let option = checkOption(func[0], txt);
      if (option) {
        roomActivity(room.name, func[0].name, func[0].status, changeStatus(func[0], option));
        console.log(option);
        console.log('Status: ' + func[0].status);
      }
    }
  }
}



// Check if there are any Rooms in the sentence
function checkRooms(txt) {
  for (let i in rooms) {
    if (txt.includes(rooms[i].name)) {
      return rooms[i];
    }
  }
}

// Check if there are any Function's in the sentence
function checkFunc(room, txt) {
  for (let i in room.func) {
    for (let j in room.func[i].name) {
      if (txt.includes(room.func[i].name[j])) {
        return [room.func[i], room.func[i].name[j]];
      }
    }
  }
}

// Check if there are any Option's in the sentence
function checkOption(func, txt) {
  for (let i in func.option) {
    if (txt.includes(func.option[i])) {
      return func.option[i];
    }
  }
}


// Change status
function changeStatus(func, stat) {
  func.status = stat;
  console.log(func.status);
  return func.status;
}



//
function roomActivity(room, func, lstat, stat) {
  $('.' + room + ' .' + func).removeClass(lstat);
  $('.' + room + ' .' + func).addClass(stat);
}




// Speech etc.
$(document).ready(function() {
  // returnSavedMessages();
  // createBotMessage();
  // markovIt();
  if (annyang) {
    annyang.start();
    let sayThis = function(result) {
      // console.log('Topic');
      // console.log(nlp(result).nouns().out('topk'));
      console.log('Msg');
      console.log(result);
      sendMessage(result);
      let doc = nlp(result).verbs().conjugate();
      console.log('Verb');
      console.log(doc);
      for (let verb in doc) {
        console.log(doc[verb].Infinitive);
      }
      console.log('----------------------------');
    }
    let commands = { '*say': sayThis  };
    annyang.addCommands(commands);
  }
});



// Speech
$(function(){
  if ('speechSynthesis' in window) {
    speechSynthesis.onvoiceschanged = function() {
      let $voicelist = $('#voices');

      if($voicelist.find('option').length == 0) {
        speechSynthesis.getVoices().forEach(function(voice, index) {
          let $option = $('<option>')
          .val(index)
          .html(voice.name + (voice.default ? ' (default)' :''));

          $voicelist.append($option);
        });

      }
    }
  }
});

function speak(message) {
  let text = message.replace(/<\/?[^>]+(>|$)/g, "");
  console.log(text);
  let msg = new SpeechSynthesisUtterance();
  let voices = window.speechSynthesis.getVoices();
  msg.voice = voices[0];
  msg.rate = 1;
  msg.pitch = 1;
  msg.text = text;

  msg.onend = function(e) {
    console.log('Finished in ' + event.elapsedTime + ' seconds.');
  };


  speechSynthesis.speak(msg);
};
































//
